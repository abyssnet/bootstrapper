import { createLibp2p } from 'libp2p'
import { noise } from '@chainsafe/libp2p-noise'
import { mplex } from '@libp2p/mplex'
import { createFromProtobuf } from '@libp2p/peer-id-factory'
import { webSockets } from '@libp2p/websockets'
import { webRTCStar } from '@libp2p/webrtc-star'
import { kadDHT } from '@libp2p/kad-dht'
import { gossipsub } from "@chainsafe/libp2p-gossipsub";
import { peerIdFromString } from "@libp2p/peer-id"
import { multiaddr } from '@multiformats/multiaddr'
import { pubsubPeerDiscovery } from '@libp2p/pubsub-peer-discovery'
import wrtc from 'wrtc'

import * as fs from 'fs';

const te = new TextEncoder()
const td = new TextDecoder()
var node

const starAddrs = [
	'/dns4/webrtc-star.nonchip.de/tcp/443/wss/p2p-webrtc-star',
	'/dns4/wrtc-star1.par.dwebops.pub/tcp/443/wss/p2p-webrtc-star',
	'/dns4/wrtc-star2.sjc.dwebops.pub/tcp/443/wss/p2p-webrtc-star',
];

const dirPeer = (id) => {
	let addrs = []
	starAddrs.forEach(element => {
		addrs.push(multiaddr(element+'/p2p/'+id))
	});
	return {
		id: peerIdFromString(id),
		addrs
	}
}


const main = async () => {
	const pbuf = fs.readFileSync('peerid.bin')
	const peerId = await createFromProtobuf(pbuf)

	const wrtcStar = webRTCStar({ wrtc })
	const dht = kadDHT({
			clientMode: false,
	})
	
	node = await createLibp2p({
		peerId,
		addresses: {
			listen: starAddrs
		},
		dht,
		transports: [wrtcStar.transport,webSockets()],
		connectionEncryption: [noise()],
		streamMuxers: [mplex()],
		pubsub: gossipsub({
			doPX: true,
			emitSelf: true,
			canRelayMessage: true,
			allowPublishToZeroPeers: true,
			directConnectTicks: 120,
			maxInboundStreams: 4,
			maxOutboundStreams: 2,
			directPeers: [/*dirPeer('12D3KooWEbDVySzmBvBVUGdyWx46qC5u2z32M41YeBXtkpYbpjVn'),*/],

		}),
		peerDiscovery: [
			wrtcStar.discovery,
			pubsubPeerDiscovery({
				topics: ['/abyssnet/0/discovery']
			}),
		],
		relay: {
			advertise: {
				enabled: true,
			},
			hop: {
				enabled: true,
			},
			autoRelay: {
				enabled: true,
			},
			enabled: true,
		},
		nat: {
			enabled: true
		},
		connectionManager: {
			autoDial: true,
			minConnections: 50,
			maxConnections: Infinity,
			maxEventLoopDelay: Infinity,
		},
	});

  // start libp2p
  await node.start()
  console.log('libp2p has started')

  // print out listening addresses
  console.log('listening on addresses:')
  node.getMultiaddrs().forEach((addr) => {
    console.log(addr.toString())
	})
	
	node.connectionManager.addEventListener('peer:connect', (evt) => {
		console.log(evt.detail.remotePeer.toString(), 'connect')
	})
	node.connectionManager.addEventListener('peer:disconnect', (evt) => {
		console.log(evt.detail.remotePeer.toString(), 'disconnect')
	})
	node.pubsub.addEventListener('gossipsub:message', (evt) => {
		if(evt.detail.msg.topic=='/abyssnet/0/ping')
			console.log(evt.detail.msg.from.toString(), td.decode(evt.detail.msg.data))
	})
	node.pubsub.subscribe('/abyssnet/0/ping')

	setInterval(() => {
		node.pubsub.publish('/abyssnet/0/ping', te.encode(Date.now().toString()+' bootstrapper'))
	}, 23e3);

}

const stop = async () => {
  // stop libp2p
  await node.stop()
  console.log('libp2p has stopped')
  process.exit(0)
}

process.on('SIGTERM', stop)
process.on('SIGINT', stop)

main().then().catch(console.error)
