import { createEd25519PeerId, exportToProtobuf } from '@libp2p/peer-id-factory'
import * as fs from 'fs';


const main = async () => {
	fs.writeFileSync('peerid.bin', exportToProtobuf(await createEd25519PeerId()))
}

main().then().catch(console.error)
